import React, { Component } from 'react';
import './App.css';
import Circle from './circle';
import Car from './images/car.png';
import User from './images/user.png';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animate: false,
    };
  }

  renderCircles = () => {
    const amount = 10;
    let rows = [];
    let transformDegree= 16;

    for (var i = 0; i < amount; i++) {
      rows.push(
        <Circle 
          index={i} 
          transformDegree={transformDegree} 
          imgSrc={i>5 ? Car : User}
          classNames={'wheelElement'}
          animate={this.state.animate}
        />
      );
      transformDegree += 36;
    }
    return rows;
  }

  toogleAnimation = () => {
    this.setState(prevState =>({animate: !prevState.animate}));
  }

  render() {
    return (
      <div className="App">
        <div className={`circle-container ${this.state.animate ? 'animation':''}`}>
          <Circle
            index={0} 
            imgSrc={Car}
            classAWrapper="center-circle-a"
            classImage="center-circle-image"
            animate={this.state.animate}
          />
          {this.renderCircles()}
        </div>
        <div className="control-class">
          <input className="call-button" type="button" value={`${this.state.animate === true ? 'Cancel':''} Call`} onClick={this.toogleAnimation.bind(this)}/>
        </div>
      </div>
    );
  }
}

export default App;
