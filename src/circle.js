import React, { Component } from 'react';
import './App.css';

class Circle extends Component {
  render() {
    const {index, transformDegree, imgSrc, classNames, classAWrapper, classImage, animate} = this.props;
    return (
        <div key={index}>
            <a 
                className={`${classNames} ${classAWrapper}`} 
                href={`${index}`} 
                style = {{'transform': `rotate(${transformDegree}deg) translate(12em) rotate(-${transformDegree}deg)`}} 
            >
                <img className={`${animate ? 'image-fixation':''} ${classImage}`} src={imgSrc} alt=""/>
            </a>
        </div>
    );
  }
}

export default Circle;
